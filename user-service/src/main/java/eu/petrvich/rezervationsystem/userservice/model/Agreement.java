package eu.petrvich.rezervationsystem.userservice.model;

import eu.petrvich.rezervationsystem.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "AGREEMENTS")
@EqualsAndHashCode(callSuper = true)
public class Agreement extends BaseEntity {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name = "PHOTO_AGREEMENT")
    private boolean photoAgreement = false;

    @Column(name = "PARENT_AGREEMENT")
    private boolean parentAgreement = false;

    @Column(name = "UPLOADED_FILE", length = 4000)
    private String uploadedFile;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "VALIDATED_BY")
    private User validatedBy;

    @Column(name = "VALIDATED_AT")
    private LocalDateTime validatedAt;
}
