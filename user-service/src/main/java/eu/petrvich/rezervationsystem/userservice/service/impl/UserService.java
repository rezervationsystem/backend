package eu.petrvich.rezervationsystem.userservice.service.impl;

import eu.petrvich.rezervationsystem.dto.usermanagement.HealthIssueDto;
import eu.petrvich.rezervationsystem.dto.usermanagement.UserDto;
import eu.petrvich.rezervationsystem.exceptions.RecordNotFoundException;
import eu.petrvich.rezervationsystem.service.impl.CrudService;
import eu.petrvich.rezervationsystem.userservice.model.User;
import eu.petrvich.rezervationsystem.userservice.repository.UserRepository;
import eu.petrvich.rezervationsystem.userservice.service.IHealthIssueService;
import eu.petrvich.rezervationsystem.userservice.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class UserService extends CrudService<UUID, UserDto, UserRepository, User> implements IUserService {

    private UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    protected UserRepository getRepository() {
        return this.repository;
    }

    @Override
    public UserDto findByEmail(String email) {
        User user = this.repository.findByEmail(email)
                .orElseThrow(() -> new RecordNotFoundException(log, "Record of entity name {} not found for primary key {}.", this.getClass().getName(), email));
        return this.toDto(user);
    }

    @Override
    protected UserDto toDto(User entity) {
        UserDto dto = super.toDto(entity);
        dto.setName(entity.getName());
        dto.setSurname(entity.getSurname());
        dto.setEmail(entity.getEmail());
        dto.setPhone(entity.getPhone());
        dto.setCountryCode(entity.getCountryCode());
        dto.setDateOfBirth(entity.getDateOfBirth());
        dto.setNote(entity.getNote());
        return dto;
    }

    @Override
    protected User toEntity(UserDto userDto) {
        return this.toEntity(userDto, new User());
    }

    @Override
    protected User toEntity(UserDto userDto, User user) {
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setEmail(userDto.getEmail());
        user.setPhone(userDto.getPhone());
        user.setCountryCode(userDto.getCountryCode());
        user.setDateOfBirth(userDto.getDateOfBirth());
        user.setNote(userDto.getNote());
        return user;
    }

    @Override
    protected UserDto getNewDto() {
        return new UserDto();
    }
}
