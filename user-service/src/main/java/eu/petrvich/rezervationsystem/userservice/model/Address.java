package eu.petrvich.rezervationsystem.userservice.model;

import eu.petrvich.rezervationsystem.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "ADDRESSES")
@EqualsAndHashCode(callSuper = true)
public class Address extends BaseEntity {

    @Column(name = "STREET_NAME")
    private String street;

    @Column(name = "HOUSE_NUMBER")
    private String houseNumber;

    @Column(name = "ORIENTATION_NUMBER")
    private String orientationNumber;

    @Column(name = "CITY")
    private String city;

    @Column(name = "COUNTRY")
    private String country;

    @OneToMany(mappedBy = "address", fetch = FetchType.LAZY)
    private List<UserAddress> userAddresses;
}
