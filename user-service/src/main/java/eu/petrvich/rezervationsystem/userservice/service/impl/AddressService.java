package eu.petrvich.rezervationsystem.userservice.service.impl;

import eu.petrvich.rezervationsystem.dto.usermanagement.AddressDto;
import eu.petrvich.rezervationsystem.service.impl.CrudService;
import eu.petrvich.rezervationsystem.userservice.model.Address;
import eu.petrvich.rezervationsystem.userservice.repository.AddressRepository;
import eu.petrvich.rezervationsystem.userservice.service.IAddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class AddressService extends CrudService<UUID, AddressDto, AddressRepository, Address> implements IAddressService {

    private AddressRepository repository;

    @Autowired
    public AddressService(AddressRepository repository) {
        this.repository = repository;
    }

    @Override
    protected AddressRepository getRepository() {
        return this.repository;
    }

    @Override
    protected AddressDto toDto(Address entity) {
        AddressDto dto = super.toDto(entity);
        dto.setStreet(entity.getStreet());
        dto.setCity(entity.getCity());
        dto.setHouseNumber(entity.getHouseNumber());
        dto.setOrientationNumber(entity.getOrientationNumber());
        dto.setCountry(entity.getCountry());
        return dto;
    }

    @Override
    protected Address toEntity(AddressDto userDto) {
        return this.toEntity(userDto, new Address());
    }

    @Override
    protected Address toEntity(AddressDto dto, Address entity) {
        entity.setStreet(dto.getStreet());
        entity.setCity(dto.getCity());
        entity.setHouseNumber(dto.getHouseNumber());
        entity.setOrientationNumber(dto.getOrientationNumber());
        entity.setCountry(dto.getCountry());
        return entity;
    }

    @Override
    protected AddressDto getNewDto() {
        return new AddressDto();
    }
}
