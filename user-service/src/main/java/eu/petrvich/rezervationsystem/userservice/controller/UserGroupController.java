package eu.petrvich.rezervationsystem.userservice.controller;

import eu.petrvich.rezervationsystem.controller.IBaseController;
import eu.petrvich.rezervationsystem.dto.usermanagement.UserGroupDto;
import eu.petrvich.rezervationsystem.userservice.service.IUserGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/users/groups")
public class UserGroupController implements IBaseController<UserGroupDto, IUserGroupService> {

    @Autowired
    private IUserGroupService service;

    @Override
    public IUserGroupService getService() {
        return this.service;
    }
}
