package eu.petrvich.rezervationsystem.userservice.model;

import eu.petrvich.rezervationsystem.entity.BaseEntity;
import eu.petrvich.rezervationsystem.enums.GroupType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "USER_GROUPS")
@EqualsAndHashCode(callSuper = true)
public class UserGroup extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE")
    private GroupType type;

    @Column(name = "NOTE", length = 4000)
    private String note;

    @OneToMany(mappedBy = "userGroup", fetch = FetchType.LAZY)
    private List<UserGroupUsers> userGroupUsers;
}
