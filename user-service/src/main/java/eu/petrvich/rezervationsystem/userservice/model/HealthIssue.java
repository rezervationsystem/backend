package eu.petrvich.rezervationsystem.userservice.model;

import eu.petrvich.rezervationsystem.entity.BaseEntity;
import eu.petrvich.rezervationsystem.enums.HealthIssueType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@Table(name = "HEALTH_ISSUE")
@EqualsAndHashCode(callSuper = true)
public class HealthIssue extends BaseEntity {

    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name = "DESCRIPTION", length = 4000)
    private String description;

    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private HealthIssueType type;
}
