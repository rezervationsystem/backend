package eu.petrvich.rezervationsystem.userservice.controller;

import eu.petrvich.rezervationsystem.controller.IBaseController;
import eu.petrvich.rezervationsystem.dto.usermanagement.AgreementDto;
import eu.petrvich.rezervationsystem.userservice.service.IAgreementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/agreements")
public class AgreementController implements IBaseController<AgreementDto, IAgreementService> {

    @Autowired
    private IAgreementService service;

    @Override
    public IAgreementService getService() {
        return this.service;
    }
}
