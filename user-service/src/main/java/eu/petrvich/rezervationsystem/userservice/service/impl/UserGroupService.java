package eu.petrvich.rezervationsystem.userservice.service.impl;

import eu.petrvich.rezervationsystem.dto.usermanagement.UserGroupDto;
import eu.petrvich.rezervationsystem.service.impl.CrudService;
import eu.petrvich.rezervationsystem.userservice.model.UserGroup;
import eu.petrvich.rezervationsystem.userservice.repository.UserGroupRepository;
import eu.petrvich.rezervationsystem.userservice.service.IUserGroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class UserGroupService extends CrudService<UUID, UserGroupDto, UserGroupRepository, UserGroup> implements IUserGroupService {

    private UserGroupRepository repository;

    @Autowired
    public UserGroupService(UserGroupRepository repository) {
        this.repository = repository;
    }

    @Override
    protected UserGroupRepository getRepository() {
        return this.repository;
    }

    @Override
    protected UserGroup toEntity(UserGroupDto dto) {
        return toEntity(dto, new UserGroup());
    }

    @Override
    protected UserGroup toEntity(UserGroupDto dto, UserGroup entity) {
        entity.setName(dto.getName());
        entity.setType(dto.getType());
        entity.setNote(dto.getNote());
        return entity;
    }

    @Override
    protected UserGroupDto getNewDto() {
        return new UserGroupDto();
    }
}