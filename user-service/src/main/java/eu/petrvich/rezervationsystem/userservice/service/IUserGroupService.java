package eu.petrvich.rezervationsystem.userservice.service;

import eu.petrvich.rezervationsystem.dto.usermanagement.UserGroupDto;
import eu.petrvich.rezervationsystem.service.ICrudService;

import java.util.UUID;

public interface IUserGroupService extends ICrudService<UUID, UserGroupDto> {

}
