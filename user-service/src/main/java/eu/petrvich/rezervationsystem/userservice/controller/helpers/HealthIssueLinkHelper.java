package eu.petrvich.rezervationsystem.userservice.controller.helpers;

import eu.petrvich.rezervationsystem.controller.helpers.ILinkHelper;
import eu.petrvich.rezervationsystem.dto.usermanagement.HealthIssueDto;
import org.springframework.stereotype.Component;

@Component
public class HealthIssueLinkHelper implements ILinkHelper<HealthIssueDto> {
}
