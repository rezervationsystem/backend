package eu.petrvich.rezervationsystem.userservice.service;

import eu.petrvich.rezervationsystem.dto.usermanagement.AddressDto;
import eu.petrvich.rezervationsystem.service.ICrudService;

import java.util.UUID;

public interface IAddressService extends ICrudService<UUID, AddressDto> {


}
