package eu.petrvich.rezervationsystem.userservice.repository;

import eu.petrvich.rezervationsystem.userservice.model.Agreement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AgreementRepository extends JpaRepository<Agreement, UUID> {
}
