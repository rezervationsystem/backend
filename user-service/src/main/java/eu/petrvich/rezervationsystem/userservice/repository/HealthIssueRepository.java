package eu.petrvich.rezervationsystem.userservice.repository;

import eu.petrvich.rezervationsystem.userservice.model.HealthIssue;
import eu.petrvich.rezervationsystem.userservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;
import java.util.stream.Stream;

public interface HealthIssueRepository extends JpaRepository<HealthIssue, UUID> {

    Stream<HealthIssue> findAllByUser(User user);
}
