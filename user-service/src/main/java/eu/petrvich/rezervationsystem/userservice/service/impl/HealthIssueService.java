package eu.petrvich.rezervationsystem.userservice.service.impl;

import eu.petrvich.rezervationsystem.dto.usermanagement.HealthIssueDto;
import eu.petrvich.rezervationsystem.exceptions.PermissionException;
import eu.petrvich.rezervationsystem.service.impl.CrudService;
import eu.petrvich.rezervationsystem.userservice.model.HealthIssue;
import eu.petrvich.rezervationsystem.userservice.model.User;
import eu.petrvich.rezervationsystem.userservice.repository.HealthIssueRepository;
import eu.petrvich.rezervationsystem.userservice.service.IHealthIssueService;
import eu.petrvich.rezervationsystem.userservice.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class HealthIssueService extends CrudService<UUID, HealthIssueDto, HealthIssueRepository, HealthIssue> implements IHealthIssueService {

    private HealthIssueRepository repository;
    private IUserService userService;

    @Autowired
    public HealthIssueService(HealthIssueRepository repository, IUserService userService) {
        this.repository = repository;
        this.userService = userService;
    }

    @Override
    public List<HealthIssueDto> getHealthIssueForUser(User user) {
        return this.repository.findAllByUser(user)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    protected HealthIssueRepository getRepository() {
        return this.repository;
    }

    @Override
    protected HealthIssue toEntity(HealthIssueDto dto) {
        HealthIssue healthIssue = toHealthIssueEntity(dto, new HealthIssue());
        User user = this.userService.findById(dto.getUserId());
        healthIssue.setUser(user);
        return healthIssue;
    }

    private HealthIssue toHealthIssueEntity(HealthIssueDto dto, HealthIssue entity) {
        entity.setDescription(dto.getDescription());
        entity.setType(dto.getType());
        entity.setName(dto.getName());
        return entity;
    }

    @Override
    protected HealthIssue toEntity(HealthIssueDto dto, HealthIssue entity) {
        if(dto.getUserId() != null && !dto.getUserId().equals(entity.getUser().getId())){
            throw new PermissionException(log, "Changing user is not allowed");
        }
        return toHealthIssueEntity(dto, entity);
    }

    @Override
    protected HealthIssueDto getNewDto() {
        return new HealthIssueDto();
    }
}
