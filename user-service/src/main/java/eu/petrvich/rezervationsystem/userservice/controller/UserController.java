package eu.petrvich.rezervationsystem.userservice.controller;

import eu.petrvich.rezervationsystem.controller.IBaseController;
import eu.petrvich.rezervationsystem.dto.usermanagement.HealthIssueDto;
import eu.petrvich.rezervationsystem.dto.usermanagement.UserDto;
import eu.petrvich.rezervationsystem.userservice.controller.helpers.HealthIssueLinkHelper;
import eu.petrvich.rezervationsystem.userservice.model.User;
import eu.petrvich.rezervationsystem.userservice.service.IHealthIssueService;
import eu.petrvich.rezervationsystem.userservice.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/users")
public class UserController implements IBaseController<UserDto, IUserService> {

    @Autowired
    private IUserService userService;

    @Autowired
    private IHealthIssueService healthIssueService;

    @Autowired
    private HealthIssueLinkHelper linkHelper;

    @GetMapping(path = "/{id}/healthIssue")
    public List<Resources<HealthIssueDto>> getHealthIssue(@PathVariable("id") UUID id) {
        User user = this.userService.findById(id);
        return this.healthIssueService.getHealthIssueForUser(user).stream()
                .map(linkHelper::toResourceWithLink)
                .collect(Collectors.toList());
    }

    public Collection<Link> getLinks(UserDto dto) {
        Collection<Link> links = this.getBaseLinks(dto);
        Link healthIssue = createLink(methodOn(UserController.class).getHealthIssue(dto.getUuid()), "healthIssue");
        links.add(healthIssue);
        return links;
    }

    @Override
    public IUserService getService() {
        return this.userService;
    }
}
