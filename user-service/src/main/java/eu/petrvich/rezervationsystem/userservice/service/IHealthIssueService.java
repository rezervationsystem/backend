package eu.petrvich.rezervationsystem.userservice.service;

import eu.petrvich.rezervationsystem.dto.usermanagement.HealthIssueDto;
import eu.petrvich.rezervationsystem.service.ICrudService;
import eu.petrvich.rezervationsystem.userservice.model.User;

import java.util.List;
import java.util.UUID;

public interface IHealthIssueService extends ICrudService<UUID, HealthIssueDto> {
    List<HealthIssueDto> getHealthIssueForUser(User user);
}
