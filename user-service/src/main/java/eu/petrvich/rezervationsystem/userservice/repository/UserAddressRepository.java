package eu.petrvich.rezervationsystem.userservice.repository;

import eu.petrvich.rezervationsystem.userservice.model.UserAddress;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserAddressRepository extends JpaRepository<UserAddress, UUID> {
}
