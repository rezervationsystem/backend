package eu.petrvich.rezervationsystem.userservice;

import eu.petrvich.rezervationsystem.configuration.ResourceServerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

@EnableEurekaClient
@SpringBootApplication
@Import(ResourceServerConfiguration.class)
public class UserServiceApplication {

    @Primary
    @Bean
    public RemoteTokenServices tokenServices() {
        final RemoteTokenServices tokenService = new RemoteTokenServices();
        tokenService.setCheckTokenEndpointUrl("http://localhost:8000/oauth/check_token");
        tokenService.setClientId("gigy");
        tokenService.setClientSecret("secret");
        return tokenService;
    }

    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }
}
