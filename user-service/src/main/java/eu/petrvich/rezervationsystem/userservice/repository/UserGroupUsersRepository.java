package eu.petrvich.rezervationsystem.userservice.repository;

import eu.petrvich.rezervationsystem.userservice.model.UserGroupUsers;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserGroupUsersRepository extends JpaRepository<UserGroupUsers, UUID> {


}
