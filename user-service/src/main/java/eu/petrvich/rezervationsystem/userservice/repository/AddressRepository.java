package eu.petrvich.rezervationsystem.userservice.repository;

import eu.petrvich.rezervationsystem.userservice.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AddressRepository extends JpaRepository<Address, UUID> {

}
