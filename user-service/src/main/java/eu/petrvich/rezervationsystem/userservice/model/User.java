package eu.petrvich.rezervationsystem.userservice.model;

import eu.petrvich.rezervationsystem.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "USERS")
@EqualsAndHashCode(callSuper = true)
public class User extends BaseEntity {

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "SURNAME", nullable = false)
    private String surname;

    @Email
    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "DATE_OF_BIRTH", nullable = false)
    private LocalDate dateOfBirth;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<UserAddress> addressList;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<UserGroupUsers> userGroups;

}