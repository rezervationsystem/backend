package eu.petrvich.rezervationsystem.userservice.controller;

import eu.petrvich.rezervationsystem.controller.IBaseController;
import eu.petrvich.rezervationsystem.dto.usermanagement.AddressDto;
import eu.petrvich.rezervationsystem.userservice.service.IAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/addresses")
public class AddressController implements IBaseController<AddressDto, IAddressService> {

    @Autowired
    private IAddressService service;

    @Override
    public IAddressService getService() {
        return this.service;
    }
}
