package eu.petrvich.rezervationsystem.userservice.service;

import eu.petrvich.rezervationsystem.dto.usermanagement.AgreementDto;
import eu.petrvich.rezervationsystem.service.ICrudService;

import java.util.UUID;

public interface IAgreementService extends ICrudService<UUID, AgreementDto> {


}
