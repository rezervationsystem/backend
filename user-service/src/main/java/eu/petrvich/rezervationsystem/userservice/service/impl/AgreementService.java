package eu.petrvich.rezervationsystem.userservice.service.impl;

import eu.petrvich.rezervationsystem.dto.usermanagement.AgreementDto;
import eu.petrvich.rezervationsystem.service.impl.CrudService;
import eu.petrvich.rezervationsystem.userservice.model.Agreement;
import eu.petrvich.rezervationsystem.userservice.model.User;
import eu.petrvich.rezervationsystem.userservice.repository.AgreementRepository;
import eu.petrvich.rezervationsystem.userservice.service.IAgreementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class AgreementService extends CrudService<UUID, AgreementDto, AgreementRepository, Agreement> implements IAgreementService {

    private AgreementRepository repository;
    private UserService userService;

    @Autowired
    public AgreementService(AgreementRepository repository, UserService userService) {
        this.repository = repository;
        this.userService = userService;
    }

    @Override
    protected AgreementRepository getRepository() {
        return this.repository;
    }

    @Override
    protected Agreement toEntity(AgreementDto dto) {
        Agreement entity = toEntity(dto, new Agreement());
        User user = this.userService.findById(dto.getUserId());
        entity.setUser(user);
        return entity;
    }

    @Override
    protected Agreement toEntity(AgreementDto dto, Agreement entity) {
        entity.setParentAgreement(dto.isParentAgreement());
        entity.setPhotoAgreement(dto.isPhotoAgreement());
        entity.setUploadedFile(dto.getUploadedFile());
        return entity;
    }

    @Override
    protected AgreementDto getNewDto() {
        return new AgreementDto();
    }
}
