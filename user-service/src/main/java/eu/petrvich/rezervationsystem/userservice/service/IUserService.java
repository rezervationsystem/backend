package eu.petrvich.rezervationsystem.userservice.service;

import eu.petrvich.rezervationsystem.dto.usermanagement.HealthIssueDto;
import eu.petrvich.rezervationsystem.dto.usermanagement.UserDto;
import eu.petrvich.rezervationsystem.service.ICrudService;
import eu.petrvich.rezervationsystem.userservice.model.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IUserService extends ICrudService<UUID, UserDto> {

    User findById(UUID id);

    UserDto findByEmail(String email);

}
