package eu.petrvich.rezervationsystem.userservice.controller;

import eu.petrvich.rezervationsystem.controller.IBaseController;
import eu.petrvich.rezervationsystem.dto.usermanagement.HealthIssueDto;
import eu.petrvich.rezervationsystem.userservice.service.IHealthIssueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(path = "/heatlhIssues")
public class HealthIssueController implements IBaseController<HealthIssueDto, IHealthIssueService> {

    @Autowired
    private IHealthIssueService service;

    @Override
    public IHealthIssueService getService() {
        return service;
    }
}
