package eu.petrvich.rezervationsystem.utils;

public interface Constant {

    String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    String DATE_FORMAT = "yyyy-MM-dd";
}
