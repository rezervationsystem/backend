package eu.petrvich.rezervationsystem.controller.helpers;

import eu.petrvich.rezervationsystem.controller.client.UserClient;
import eu.petrvich.rezervationsystem.dto.BaseDto;
import eu.petrvich.rezervationsystem.dto.usermanagement.UserDto;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public interface ILinkHelper<U extends BaseDto> {

    default Link createLink(Object method, String rel) {
        return linkTo(method).withRel(rel);
    }

    default Link getUserLink(UUID id, String rel) {
        Resources<UserDto> user = methodOn(UserClient.class).getUser(id);
        return this.createLink(user, rel);
    }

    default Collection<Link> getLinks(U dto){
        return getBaseLinks(dto);
    }

    default Collection<Link> getBaseLinks(U dto) {
        Collection<Link> links = new ArrayList<>();
        Link selfRel = getUserLink(dto.getUuid(), "self");
        links.add(selfRel);

        if (dto.getCreatedBy() != null) {
            Link createdByRel = getUserLink(dto.getCreatedBy(), "created");
            links.add(createdByRel);
        }

        if (dto.getUpdatedBy() != null) {
            Link updatedByRel = getUserLink(dto.getUpdatedBy(), "updated");
            links.add(updatedByRel);
        }
        return links;
    }

    default Resources<U> toResourceWithLink(U dto) {
        return new Resources<>(Collections.singleton(dto), getLinks(dto));
    }
}
