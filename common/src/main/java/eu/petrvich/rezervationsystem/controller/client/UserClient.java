package eu.petrvich.rezervationsystem.controller.client;

import eu.petrvich.rezervationsystem.dto.usermanagement.UserCredentialDto;
import eu.petrvich.rezervationsystem.dto.usermanagement.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

@FeignClient("user-service")
public interface UserClient {

    @GetMapping(path = "/users/{id}")
    Resources<UserDto> getUser(@PathVariable("id") UUID id);

    @GetMapping(path = "/users/credentials/login/{userName}")
    UserCredentialDto getUserByUserName(@PathVariable("userName") String userName);
}