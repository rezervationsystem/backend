package eu.petrvich.rezervationsystem.controller;

import eu.petrvich.rezervationsystem.controller.helpers.ILinkHelper;
import eu.petrvich.rezervationsystem.dto.BaseDto;
import eu.petrvich.rezervationsystem.service.ICrudService;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public interface IBaseController<U extends BaseDto, S extends ICrudService<UUID, U>> extends ILinkHelper<U> {

    S getService();

    @GetMapping
    default List<Resources<U>> getAll() {
        return this.getService().getAllRecords().stream()
                .map(this::toResourceWithLink)
                .collect(Collectors.toList());
    }

    @GetMapping(path = "/{id}")
    default Resources<U> read(@PathVariable("id") UUID id) {
        U dto = this.getService().getById(id);
        return this.toResourceWithLink(dto);
    }

    @PostMapping
    default Resources<U> create(@RequestBody U dto) {
        U responseDto = this.getService().create(dto);
        return this.toResourceWithLink(responseDto);
    }

    @PutMapping(path = "/{id}")
    default Resources<U> update(@PathVariable("id") UUID id, @RequestBody U dto) {
        U responseDto = this.getService().update(dto, id);
        return this.toResourceWithLink(responseDto);
    }


    @DeleteMapping(path = "/{id}")
    default Resources<U> delete(@PathVariable("id") UUID id) {
        U responseDto = this.getService().delete(id);
        return this.toResourceWithLink(responseDto);
    }

}
