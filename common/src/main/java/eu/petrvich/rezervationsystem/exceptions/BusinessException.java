package eu.petrvich.rezervationsystem.exceptions;

import org.slf4j.Logger;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

import java.util.Collections;

public class BusinessException extends RuntimeException {

    public BusinessException(Logger logger, String message, Object... params) {
        super(prepareMessage(logger, message, params));
    }

    public BusinessException(Logger logger, Throwable cause, String message, Object... params) {
        super(prepareMessage(logger, cause, message, params), cause);
    }

    public BusinessException(Logger logger, String message) {
        super(prepareMessage(logger, message, Collections.emptyEnumeration()));
    }

    public BusinessException(Logger logger, Throwable cause, String message) {
        super(prepareMessage(logger, cause, message, Collections.emptyEnumeration()), cause);
    }

    /**
     * Prepare error message and substitute all params to one string. Create error log using input logger instance.
     *
     * @param logger
     * @param cause
     * @param message
     * @param params
     * @return
     */
    private static String prepareMessage(Logger logger, Throwable cause, String message, Object... params) {
        FormattingTuple there = MessageFormatter.arrayFormat(message, params, cause);
        String msg = there.getMessage();

        logger.error(msg);
        return msg;
    }

    /**
     * Prepare error message and substitute all params to one string. Create error log using input logger instance.
     *
     * @param logger
     * @param message
     * @param params
     * @return
     */
    private static String prepareMessage(Logger logger, String message, Object... params) {
        return prepareMessage(logger, null, message, params);
    }
}

