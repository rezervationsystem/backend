package eu.petrvich.rezervationsystem.exceptions;

import org.slf4j.Logger;

public class RecordNotFoundException extends BusinessException {

    public RecordNotFoundException(Logger logger, String message, Object... params) {
        super(logger, message, params);
    }

    public RecordNotFoundException(Logger logger, Throwable cause, String message, Object... params) {
        super(logger, cause, message, params);
    }

    public RecordNotFoundException(Logger logger, String message) {
        super(logger, message);
    }

    public RecordNotFoundException(Logger logger, Throwable cause, String message) {
        super(logger, cause, message, cause);
    }

}

