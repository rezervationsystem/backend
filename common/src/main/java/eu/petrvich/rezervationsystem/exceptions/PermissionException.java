package eu.petrvich.rezervationsystem.exceptions;

import org.slf4j.Logger;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

public class PermissionException extends BusinessException {

    public PermissionException(Logger logger, String message, Object... params) {
        super(logger, message, params);
    }

    public PermissionException(Logger logger, Throwable cause, String message, Object... params) {
        super(logger, cause, message, params);
    }

    public PermissionException(Logger logger, String message) {
        super(logger, message);
    }

    public PermissionException(Logger logger, Throwable cause, String message) {
        super(logger, cause, message, cause);
    }

}

