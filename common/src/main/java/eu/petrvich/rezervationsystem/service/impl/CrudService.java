package eu.petrvich.rezervationsystem.service.impl;

import eu.petrvich.rezervationsystem.dto.BaseDto;
import eu.petrvich.rezervationsystem.entity.BaseEntity;
import eu.petrvich.rezervationsystem.exceptions.RecordNotFoundException;
import eu.petrvich.rezervationsystem.service.ICrudService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public abstract class CrudService<P, D extends BaseDto, R extends JpaRepository<E, P>, E extends BaseEntity> implements ICrudService<P, D> {

    protected abstract R getRepository();

    @Override
    public List<D> getAllRecords() {
        return this.getRepository()
                .findAll().stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public D getById(P primaryKey) {
        E entity = this.findById(primaryKey);
        return this.toDto(entity);
    }

    public E findById(P primaryKey) {
        return this.getRepository()
                .findById(primaryKey)
                .orElseThrow(() -> new RecordNotFoundException(log, "Record of entity name {} not found for primary key {}.", this.getClass().getName(), primaryKey));
    }

    @Override
    public D create(D data) {
        E entity = this.toEntity(data);
        E save = this.getRepository().save(entity);
        return this.toDto(save);
    }

    @Override
    public D update(D data, P primaryKey) {
        E original = this.findById(primaryKey);
        E entity = this.toEntity(data, original);
        E save = this.getRepository().save(entity);
        return this.toDto(save);
    }

    @Override
    public D delete(P primaryKey) {
        E entity = this.findById(primaryKey);
        this.getRepository().delete(entity);
        return toDto(entity);
    }

    protected D toDto(E entity) {
        D dto = getNewDto();
        dto.setUuid(entity.getId());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
        dto.setCreatedBy(entity.getCreatedBy());
        dto.setUpdatedBy(entity.getUpdatedBy());

        return dto;
    }

    protected abstract E toEntity(D dto);

    /* TODO protect changes of change owner or date */
    //        if(!dto.getCreatedBy().equals(entity.getId())){
    //            throw new PermissionException(log, "Change owner is not allowed for entity name {} and original entity primary key is {}", this.getClass().getName(), entity.getId())
    //        }
    protected abstract E toEntity(D dto, E entity);

    protected abstract D getNewDto();
}
