package eu.petrvich.rezervationsystem.service;

import eu.petrvich.rezervationsystem.dto.BaseDto;

import java.util.List;

public interface ICrudService<P, U extends BaseDto> {

    List<U> getAllRecords();
    U getById(P primaryKey);
    U create(U data);
    U update(U data, P primaryKey);
    U delete(P primaryKey);

}
