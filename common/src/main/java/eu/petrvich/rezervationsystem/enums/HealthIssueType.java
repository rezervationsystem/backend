package eu.petrvich.rezervationsystem.enums;

public enum HealthIssueType {

    ALLERGY, DIET, OTHER, PHYSICAL_PROBLEM;
}