package eu.petrvich.rezervationsystem.enums;

public enum GroupType {

    FAMILY, ANIMATOR, SCHOOL;
}