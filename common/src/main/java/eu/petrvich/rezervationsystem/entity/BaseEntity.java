package eu.petrvich.rezervationsystem.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@MappedSuperclass
public class BaseEntity {

    @Id
    @Column(name = "ID")
    private UUID id;

    @Column(name = "CREATED_BY")
    private UUID createdBy;

    @Column(name = "UPDATED_BY")
    private UUID updatedBy;

    @Column(name = "CREATED_AT")
    private LocalDateTime createdAt;

    @Column(name = "UPDATED_AT")
    private LocalDateTime updatedAt;

    @PrePersist
    public void beforeCreate(){
        this.id = UUID.randomUUID();
        this.createdAt = LocalDateTime.now();
    }

    @PreUpdate
    public void beforeUpdate(){
        this.updatedAt = LocalDateTime.now();
    }
}
