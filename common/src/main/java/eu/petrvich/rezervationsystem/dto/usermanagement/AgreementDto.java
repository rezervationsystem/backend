package eu.petrvich.rezervationsystem.dto.usermanagement;

import com.fasterxml.jackson.annotation.JsonFormat;
import eu.petrvich.rezervationsystem.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.UUID;

import static eu.petrvich.rezervationsystem.utils.Constant.DATE_TIME_FORMAT;

@Data
@EqualsAndHashCode(callSuper = true)
public class AgreementDto extends BaseDto {

    private boolean photoAgreement;

    private boolean parentAgreement;

    private String uploadedFile;

    private UUID validatedBy;

    @JsonFormat(pattern = DATE_TIME_FORMAT)
    private LocalDateTime validatedAt;

    private UUID userId;

}
