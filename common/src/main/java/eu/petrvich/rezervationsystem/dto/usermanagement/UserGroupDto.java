package eu.petrvich.rezervationsystem.dto.usermanagement;

import eu.petrvich.rezervationsystem.dto.BaseDto;
import eu.petrvich.rezervationsystem.enums.GroupType;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserGroupDto extends BaseDto {

    private String name;

    private GroupType type;

    private String note;
}
