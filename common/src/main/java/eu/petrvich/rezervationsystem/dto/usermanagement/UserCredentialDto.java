package eu.petrvich.rezervationsystem.dto.usermanagement;

import eu.petrvich.rezervationsystem.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserCredentialDto extends BaseDto implements UserDetails {

    private String username;

    private String password;

    private UUID userId;

    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;

    private List<RoleDto> authorities;
}
