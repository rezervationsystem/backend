package eu.petrvich.rezervationsystem.dto.usermanagement;

import eu.petrvich.rezervationsystem.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserDto extends BaseDto {

    private String name;

    private String surname;

    private String email;

    private String phone;

    private String countryCode = "+420";

    private String note;

    private LocalDate dateOfBirth;

}
