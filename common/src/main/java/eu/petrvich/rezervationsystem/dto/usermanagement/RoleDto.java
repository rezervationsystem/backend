package eu.petrvich.rezervationsystem.dto.usermanagement;

import eu.petrvich.rezervationsystem.dto.BaseDto;
import jdk.nashorn.internal.ir.annotations.Reference;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RoleDto extends BaseDto implements GrantedAuthority {

    @NonNull
    private String authority;

}
