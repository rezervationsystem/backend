package eu.petrvich.rezervationsystem.dto.usermanagement;

import eu.petrvich.rezervationsystem.dto.BaseDto;
import eu.petrvich.rezervationsystem.enums.HealthIssueType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
public class HealthIssueDto extends BaseDto {

    private String name;

    private String description;

    private HealthIssueType type;

    private UUID userId;
}
