package eu.petrvich.rezervationsystem.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

import static eu.petrvich.rezervationsystem.utils.Constant.DATE_TIME_FORMAT;

@Data
public class BaseDto {

    private UUID uuid;

    @JsonFormat(pattern = DATE_TIME_FORMAT)
    private LocalDateTime createdAt;

    @JsonFormat(pattern = DATE_TIME_FORMAT)
    private LocalDateTime updatedAt;

    private UUID createdBy;

    private UUID updatedBy;

}
