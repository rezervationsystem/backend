package eu.petrvich.rezervationsystem.authorizationserver.model;

import eu.petrvich.rezervationsystem.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "ROLES")
@EqualsAndHashCode(callSuper = true)
public class Role extends BaseEntity {

    private String name;

    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY)
    private List<UserRole> userRoles;
}
