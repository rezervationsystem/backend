package eu.petrvich.rezervationsystem.authorizationserver.service;

import eu.petrvich.rezervationsystem.dto.usermanagement.RoleDto;
import eu.petrvich.rezervationsystem.service.ICrudService;

import java.util.UUID;

public interface IRoleService extends ICrudService<UUID, RoleDto> {


}
