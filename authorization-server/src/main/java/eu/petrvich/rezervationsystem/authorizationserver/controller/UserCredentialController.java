package eu.petrvich.rezervationsystem.authorizationserver.controller;

import eu.petrvich.rezervationsystem.authorizationserver.service.IUserCredentialService;
import eu.petrvich.rezervationsystem.controller.IBaseController;
import eu.petrvich.rezervationsystem.dto.usermanagement.UserCredentialDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/users/credentials")
public class UserCredentialController implements IBaseController<UserCredentialDto, IUserCredentialService> {

    @Autowired
    private IUserCredentialService service;

    @Override
    public IUserCredentialService getService() {
        return this.service;
    }

    @GetMapping(path = "/login/{userName}")
    public UserCredentialDto getUser(@PathVariable("userName") String login) {
        return this.service.findByUserName(login);
    }
}
