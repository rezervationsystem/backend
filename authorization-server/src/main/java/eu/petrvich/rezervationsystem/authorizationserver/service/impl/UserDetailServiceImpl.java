package eu.petrvich.rezervationsystem.authorizationserver.service.impl;

import eu.petrvich.rezervationsystem.authorizationserver.service.IUserCredentialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    private IUserCredentialService credentialService;

    @Autowired
    public UserDetailServiceImpl(IUserCredentialService credentialService) {
        this.credentialService = credentialService;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        return credentialService.findByUserName(userName);
    }
}