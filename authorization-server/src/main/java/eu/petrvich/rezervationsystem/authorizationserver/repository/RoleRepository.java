package eu.petrvich.rezervationsystem.authorizationserver.repository;

import eu.petrvich.rezervationsystem.authorizationserver.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
}
