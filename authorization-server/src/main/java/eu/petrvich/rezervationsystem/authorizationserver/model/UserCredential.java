package eu.petrvich.rezervationsystem.authorizationserver.model;

import eu.petrvich.rezervationsystem.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "USERS")
@EqualsAndHashCode(callSuper = true)
public class UserCredential extends BaseEntity {

    @Column(name = "LOGIN", nullable = false)
    private String login;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "ENABLED", nullable = false)
    private boolean enabled;

    @OneToMany(mappedBy = "userCredential", fetch = FetchType.EAGER)
    private List<UserRole> userRoles;

    @Column(name = "USER_ID", nullable = false)
    private UUID userId;
}
