package eu.petrvich.rezervationsystem.authorizationserver.service.impl;

import eu.petrvich.rezervationsystem.authorizationserver.model.UserCredential;
import eu.petrvich.rezervationsystem.authorizationserver.model.UserRole;
import eu.petrvich.rezervationsystem.authorizationserver.repository.UserCredentialRepository;
import eu.petrvich.rezervationsystem.authorizationserver.service.IUserCredentialService;
import eu.petrvich.rezervationsystem.controller.client.UserClient;
import eu.petrvich.rezervationsystem.dto.usermanagement.RoleDto;
import eu.petrvich.rezervationsystem.dto.usermanagement.UserCredentialDto;
import eu.petrvich.rezervationsystem.exceptions.RecordNotFoundException;
import eu.petrvich.rezervationsystem.service.impl.CrudService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserCredentialService extends CrudService<UUID, UserCredentialDto, UserCredentialRepository, UserCredential> implements IUserCredentialService {

    private UserCredentialRepository repository;

    @Autowired
    public UserCredentialService(UserCredentialRepository repository) {
        this.repository = repository;
    }

    @Override
    protected UserCredentialRepository getRepository() {
        return this.repository;
    }

    @Override
    protected UserCredential toEntity(UserCredentialDto dto) {
        return toEntity(dto, new UserCredential());
    }

    @Override
    protected UserCredential toEntity(UserCredentialDto dto, UserCredential entity) {
        entity.setLogin(dto.getUsername());
        entity.setPassword(dto.getPassword());
        entity.setUserId(dto.getUserId());
        return entity;
    }

    @Override
    protected UserCredentialDto toDto(UserCredential entity) {
        UserCredentialDto dto = super.toDto(entity);
        dto.setUsername(entity.getLogin());
        dto.setPassword(entity.getPassword());
        dto.setEnabled(entity.isEnabled());
        dto.setAccountNonExpired(true);
        dto.setAccountNonLocked(true);
        dto.setCredentialsNonExpired(true);

        List<RoleDto> roleDtoList = entity.getUserRoles().stream()
                .map(UserRole::getRole)
                .map(role -> new RoleDto(role.getName()))
                .collect(Collectors.toList());
        dto.setAuthorities(roleDtoList);

        return dto;
    }

    @Override
    public UserCredentialDto findByUserName(String login) {
        return this.repository.findByLogin(login)
                .map(this::toDto)
                .orElseThrow(() -> new RecordNotFoundException(log, "UserLogin not found for username {}.", this.getClass().getName(), login));
    }

    @Override
    protected UserCredentialDto getNewDto() {
        return new UserCredentialDto();
    }
}
