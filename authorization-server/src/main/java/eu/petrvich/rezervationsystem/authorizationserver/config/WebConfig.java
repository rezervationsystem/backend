package eu.petrvich.rezervationsystem.authorizationserver.config;

import eu.petrvich.rezervationsystem.authorizationserver.config.properties.AuthProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

/**
 * <p>
 * WebConfig class.
 * </p>
 * 
 * @author Martin Myslik
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    // @formatter:off
    registry
      .addMapping("/**")
      .allowedOrigins("*")
      .allowedHeaders("*")
      .allowedMethods("*")
      .allowCredentials(true);
    // @formatter:on
  }

}
