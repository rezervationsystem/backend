package eu.petrvich.rezervationsystem.authorizationserver.controller;

import eu.petrvich.rezervationsystem.authorizationserver.service.IRoleService;
import eu.petrvich.rezervationsystem.controller.IBaseController;
import eu.petrvich.rezervationsystem.dto.usermanagement.RoleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/roles")
public class RoleController implements IBaseController<RoleDto, IRoleService> {

    @Autowired
    private IRoleService service;

    @Override
    public IRoleService getService() {
        return this.service;
    }
}
