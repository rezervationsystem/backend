package eu.petrvich.rezervationsystem.authorizationserver.service;

import eu.petrvich.rezervationsystem.dto.usermanagement.UserCredentialDto;
import eu.petrvich.rezervationsystem.service.ICrudService;

import java.util.UUID;

public interface IUserCredentialService extends ICrudService<UUID, UserCredentialDto> {

    UserCredentialDto findByUserName(String login);
}
