package eu.petrvich.rezervationsystem.authorizationserver.repository;

import eu.petrvich.rezervationsystem.authorizationserver.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRoleRepository extends JpaRepository<UserRole, UUID> {
}
