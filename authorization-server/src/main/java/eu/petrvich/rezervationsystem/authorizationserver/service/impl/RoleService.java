package eu.petrvich.rezervationsystem.authorizationserver.service.impl;

import eu.petrvich.rezervationsystem.authorizationserver.model.Role;
import eu.petrvich.rezervationsystem.authorizationserver.repository.RoleRepository;
import eu.petrvich.rezervationsystem.authorizationserver.service.IRoleService;
import eu.petrvich.rezervationsystem.dto.usermanagement.RoleDto;
import eu.petrvich.rezervationsystem.service.impl.CrudService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class RoleService extends CrudService<UUID, RoleDto, RoleRepository, Role> implements IRoleService {

    private RoleRepository repository;

    @Autowired
    public RoleService(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    protected RoleRepository getRepository() {
        return repository;
    }

    @Override
    protected Role toEntity(RoleDto dto) {
        return this.toEntity(dto, new Role());
    }

    @Override
    protected Role toEntity(RoleDto dto, Role entity) {
        entity.setName(dto.getAuthority());
        return entity;
    }

    @Override
    protected RoleDto getNewDto() {
        return new RoleDto();
    }

    @Override
    protected RoleDto toDto(Role entity) {
        RoleDto roleDto = super.toDto(entity);
        roleDto.setAuthority(entity.getName());
        return roleDto;
    }
}
