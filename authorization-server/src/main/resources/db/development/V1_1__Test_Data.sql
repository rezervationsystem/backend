INSERT INTO users (id, created_by, updated_by, created_at, updated_at, login, password, enabled, user_id) VALUES
	('c664b254-f647-42de-913b-0433a5974d61', 'c664b254-f647-42de-913b-0433a5974d63', 'c664b254-f647-42de-913b-0433a5974d63', null, null, 'john@example.com', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', true, 'c664b254-f647-42de-913b-0433a5974d63'),
	('c664b254-f647-42de-913b-0433a5974d62', 'c664b254-f647-42de-913b-0433a5974d63', 'c664b254-f647-42de-913b-0433a5974d63', null, null, 'john2@example.com', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', true, 'c664b254-f647-42de-913b-0433a5974d63'),
	('c664b254-f647-42de-913b-0433a5974d63', 'c664b254-f647-42de-913b-0433a5974d63', 'c664b254-f647-42de-913b-0433a5974d63', null, null, 'john3@example.com', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', true, 'c664b254-f647-42de-913b-0433a5974d63');

INSERT INTO oauth_client_details (client_id, resource_ids, client_secret, scope,
authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity,
refresh_token_validity, additional_information, autoapprove) VALUES
	('gigy', 'resource', '$2a$10$s7rR9qxaUnOJaE3J6ZSICupQxm.xTJhvScmXV.ylsI3AIY5OMXp.q', 'read,write', 'authorization_code,refresh_token,implicit,password,client_credentials', '', '', 3600, 2592000, '{}', 'true');